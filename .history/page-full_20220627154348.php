<?php
/**
 * Template Name: Page (Full width)
 * Description: Page template full width
 *
 */

get_header();
?>
	<?php
	while ( have_posts() ) :
		the_post();

		the_content();
	endwhile;
	?>
<?php
get_footer();
