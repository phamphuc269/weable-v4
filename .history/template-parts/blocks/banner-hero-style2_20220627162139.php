<?php

/**
 * "Banner Hero" Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'tpa-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'my-3 w-block w-hero-modules-style-2';

if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}

$isFullWidth = false;
if( !empty($block['align']) ) {
    $isFullWidth = $block['align'] === 'full' ? true : false;
    $className .= ' align-' . $block['align'];
}
?>
<h3>HERO MODULE STYLE 2</h3>
<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="background" style="background-image: url(<?php echo get_field('background'); ?>);"></div>
    <div class="container">
        <div class="row ">
            <div class="col-lg-5">
                <div class="content">
                    <h1 class="title"><?php echo get_field('title'); ?></h1>
                    <p class="desc">
                         <?php echo get_field('desc'); ?>
                    </p>
                    <div class="btns">
                        <a href="" class="btn btn-primary mr-md-5 ">Tìm hiểu doanh nghiệp</a>
                        <a href="" class="btn btn-info ">Xem profile công ty</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="images">
                    <img src="<?php echo get_field('image'); ?>" alt="">
                </div>
            </div>
        </div>
    </div>
</section>