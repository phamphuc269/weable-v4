<?php

/**
 * "Banner Hero" Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'tpa-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'my-3 w-block w-hero-modules-style-1';

if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}

$isFullWidth = false;
if( !empty($block['align']) ) {
    $isFullWidth = $block['align'] === 'full' ? true : false;
    $className .= ' align-' . $block['align'];
}
?>
<h3>HERO MODULE STYLE 1</h3>
<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="logo">
                    <img src="<?php echo get_field('logo', 'option'); ?>" alt="">
                </div>
                <div class="content text-center">
                    <h1 class="title"><?php echo get_field('title'); ?></h1>
                    <p class="desc">
                        <?php echo get_field('desc'); ?>
                    </p>
                    <a href="<?php echo get_field('link'); ?>" class="btn btn-primary">Về Weable Team</a>
                </div>
                <div class="images">
                    <?php $item = get_field('list-image') 
                     if ($item): ?>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="img-wrap">
                                <img src="<?php echo $item['image_one']; ?>" alt="">
                            </div>
                        </div>
                        <div class="col-lg-4 mt-5">
                            <div class="img-wrap">
                                <img src="<?php echo $item['image_two']; ?>" alt="">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="img-wrap">
                                <img src="<?php echo $item['image_three']; ?>" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>