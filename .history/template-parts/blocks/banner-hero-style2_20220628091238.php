<?php

/**
 * "Banner Hero" Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'tpa-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'my-3 w-block w-hero-modules-style-2';

if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}

$isFullWidth = false;
if( !empty($block['align']) ) {
    $isFullWidth = $block['align'] === 'full' ? true : false;
    $className .= ' align-' . $block['align'];
}

$background = get_field('background');
$title = get_field('title');
$desc =  get_field('desc');
$button_left = get_field('button_left');
$button_right = get_field('button_right');
$image = get_field('image');

?>
<h3>HERO MODULE STYLE 2</h3>
<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="background" style="background-image: url(<?php echo get_field('background'); ?>);"></div>
    <div class="container">
        <div class="row ">
            <div class="col-lg-5">
                <div class="content">
                    <?= ($title) ? '<h1 class="title">'.$title.'</h1>' : '' ?>
                    <?= ($desc) ? '<p class="desc">'.$desc.'</p>' : '' ?>
                    <?php  if ($buttons) : ?>
                        <div class="btns">
                            <a class="btn btn-primary mr-md-5" href="<?= $buttons['left'] ?>" target="<?= $buttons['target'] ?>"><?= $buttons['title'] ?></a>
                            <a class="btn btn-info" href="<?= $buttons['url'] ?>" target="<?= $buttons['target'] ?>"><?= $buttons['title'] ?></a>

                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-lg-7">
                <?php  if ($image) : ?>
                    <div class="images">
                        <?= wp_get_attachment_image( $image,'full' ); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>