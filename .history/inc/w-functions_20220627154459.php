<?php
/**
 *  Add Except for pages
 */
add_action( 'init', 'add_excerpts_to_pages' );
function add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

/**
 *  SMTP Config
 */
add_action( 'phpmailer_init', function( $phpmailer ) {

    $smtp_account = get_field('smtp_account','option');
    $mail_title = get_field('mail_title','option');
    
    if ( !is_object( $phpmailer && $smtp_account ) )
    $phpmailer = (object) $phpmailer;
    $phpmailer->Mailer     = 'smtp';
    $phpmailer->Host       = 'smtp.gmail.com';
    $phpmailer->SMTPAuth   = 1;
    $phpmailer->Port       = 587;
    $phpmailer->Username   = $smtp_account['gmail'];
    $phpmailer->Password   = $smtp_account['pass_app'];
    $phpmailer->SMTPSecure = 'TLS';
    $phpmailer->From       = $smtp_account['gmail'];
    $phpmailer->FromName   = $mail_title;
});

/**
 *  Color paletes
 */

add_action('wp_head', 'w_colors', 100);
function w_colors() { 
    $colors = get_field('pallete','option');
    $primary_color = $colors['primary'];
    $warning_color = $colors['warning'];
    $danger_color = $colors['danger'];
    $success_color = $colors['success'];
    $orange_color = $colors['orange'];
?>
    <style>
        :root {
            --primary: <?= ($primary_color) ? $primary_color : '#007bff' ?>;
            --warning: <?= ($warning_color) ? $warning_color : '#ffc107' ?>;
            --danger: <?= ($danger_color) ? $danger_color : '#dc3545' ?>;
            --success: <?= ($success_color) ? $success_color : '#28a745' ?>;
            --orange: <?= ($orange_color) ? $orange_color : '#fd7e14' ?>;
        }
        .btn-primary, .wp-block-button .wp-block-button__link {
            background-color: var(--primary);
            border-color: var(--primary);
        }
    </style>
<?php }

