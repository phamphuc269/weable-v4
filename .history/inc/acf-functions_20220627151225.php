<?php

// load_json
function acf_json_load_point($paths)
{
    // remove original path (optional)
    unset($paths[0]);

    // append path
    $paths[] = get_template_directory_uri() . '/acf-json';

    // return
    return $paths;
}
add_filter('acf/settings/load_json', 'acf_json_load_point');

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Cấu hình website',
		'menu_title'	=> 'Cấu hình website',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
        'icon_url' => 'dashicons-superhero-alt',
	));

}

/**
 * Blocks.
 */
add_action('acf/init', 'tpa_acf_init_block_types');
function tpa_acf_init_block_types() {
    // Check function exists.
    if( function_exists('acf_register_block_type') ) {
        acf_register_block_type(array(
            'name'              => 'banner-hero',
            'title'             => __('Banner Hero'),
            'description'       => __('Slider'),
            'render_template'   => 'template-parts/blocks/banner-hero.php',
            'category'          => 'formatting',
            'icon'              => 'block-default',
            'mode'              => 'edit',
            'align'             => 'full',
            'keywords'          => array( 'Banner Hero', 'acf' ),
        ));
    }
}